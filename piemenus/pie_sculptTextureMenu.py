# -*- coding: utf-8 -*-
 
# Copyright (c) 2010, Dan Eicher.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
# <pep8 compliant>

import bpy
from pie_menu import MenuItem, PieMenu

import math
import blf
import bgl
 
bl_info = {
    "name": "Pie: SculptTexture Menu",
    "author": "Dan Eicher, Sean Olson",
    "version": (0, 1, 0),
    "blender": (2, 6, 4),
    "location": "View3D",
    "description": "Alphas pie menu",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "3D View"
}
 
class VIEW3D_MT_SculptTexture_Menu(bpy.types.Operator):
    '''Sculpt Texture Menu'''
    bl_idname = "view3d.sculpttexture_menu"
    bl_label = "Pie SculptTexture Menu"
 
    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D'
        

 
    def modal(self, context, event):
        context.area.tag_redraw()
 
        if event.type == 'MOUSEMOVE':
            # mouse-over highlight
            self.current = self.menu.calc_by_angle(event.mouse_region_x, event.mouse_region_y)
                        
            # pointer of center circle points at the mouse
            self.menu.pointerangle=math.degrees(math.atan2(event.mouse_region_y-self.menu.menu_y, event.mouse_region_x-self.menu.menu_x))-90     
            

        elif event.type == 'LEFTMOUSE':
            # get menu item clicked
            self.current = self.menu.calc_by_angle(event.mouse_region_x,
                                          event.mouse_region_y)
 
            do_exec = self.current and self.current.poll(context)
            if do_exec:
                self.current.op(self, context)
 
            if self.current is None or do_exec:
                context.region.callback_remove(self._handle)
                return {'FINISHED'}
 
        if self.current == -1 or event.type in ('RIGHTMOUSE', 'ESC'):
            context.region.callback_remove(self._handle)
            return {'CANCELLED'}
 
        return {'RUNNING_MODAL'}
 
    def invoke(self, context, event):
        self.current = None

        # generate menu content
        self.menu = menu_init(PieMenu(x=event.mouse_region_x,
                                      y=event.mouse_region_y,
                                      layout_radius=80,
                                      text_size=11,
                                      text_dpi=72,
                                      center_radius_squared=225,
                                      max_radius_squared=62500#22500
                                      ))
 
        context.window_manager.modal_handler_add(self)
        self._handle = context.region.callback_add(self.menu.draw,
                                                   (self, context),
                                                   'POST_PIXEL')
        return {'RUNNING_MODAL'}
 
def menu_init(menu):
   
    menu.menu_items.append(Random("Random", 0, 35))
    menu.menu_items.append(Rake("Rake", 85, 0))
    menu.menu_items.append(ThreeDimensional("3d", 70, -23))
    menu.menu_items.append(Tiled("Tiled", 55, -45, 157.5))
    menu.menu_items.append(AreaPlane("Area Plane", -55, -45))  
    menu.menu_items.append(ViewPlane("View Plane", -70, -23))  
    menu.menu_items.append(User("User", -85, 0))

 
    menu.item_offset = math.pi/8
    menu.layout_predefined()
 
    return menu
 
 
class Random(MenuItem):
    def op(self, parent, context):
        bpy.context.tool_settings.sculpt.brush.texture_angle_source_random='RANDOM'
 
 
class Rake(MenuItem):
    def op(self, parent, context):
        bpy.context.tool_settings.sculpt.brush.texture_angle_source_no_random='RAKE'

 
class User(MenuItem):
    def op(self, parent, context):
         bpy.context.tool_settings.sculpt.brush.texture_angle_source_no_random='USER'

 
class ViewPlane(MenuItem):
    def op(self, parent, context):
        bpy.context.tool_settings.sculpt.brush.texture_slot.map_mode='VIEW_PLANE'

class AreaPlane(MenuItem):
    def op(self, parent, context):
        bpy.context.tool_settings.sculpt.brush.texture_slot.map_mode='AREA_PLANE'

class Tiled(MenuItem):
    def op(self, parent, context):
        bpy.context.tool_settings.sculpt.brush.texture_slot.map_mode='TILED'

class ThreeDimensional(MenuItem):
    def op(self, parent, context):
        bpy.context.tool_settings.sculpt.brush.texture_slot.map_mode='3D'


def setBind():
    #enable the addon keybinding
    km = bpy.context.window_manager.keyconfigs.active.keymaps['Sculpt']
    for kmi in km.keymap_items:
        if kmi.idname == 'view3d.sculpttexture_menu':
            if kmi.type == 'T' and kmi.ctrl==True and kmi.alt==False and kmi.shift==False and kmi.oskey==False and kmi.any==False and kmi.key_modifier=='NONE':
                kmi.active=True
                break


def removeBind():
    #disable the addon keybinding
    km = bpy.context.window_manager.keyconfigs.active.keymaps['Sculpt']
    for kmi in km.keymap_items:
        if kmi.idname == 'view3d.sculpttexture_menu':
            if kmi.type == 'T' and kmi.ctrl==True and kmi.alt==False and kmi.shift==False and kmi.oskey==False and kmi.any==False and kmi.key_modifier=='NONE':
                kmi.active=False
                break

       
def register():
    bpy.utils.register_class(VIEW3D_MT_SculptTexture_Menu)


     #add the keybinding   
    km = bpy.context.window_manager.keyconfigs.active.keymaps['Sculpt']
    km.keymap_items.new('view3d.sculpttexture_menu', 'T', 'PRESS', ctrl=True)

 
 
def unregister():
    bpy.utils.unregister_class(VIEW3D_MT_SculptTexture_Menu)



    km = bpy.context.window_manager.keyconfigs.active.keymaps['Sculpt']
    for kmi in km.keymap_items:
        if kmi.idname == 'view3d.sculpttexture_menu':
            km.keymap_items.remove(kmi)
            break
 
if __name__ == "__main__":
    register()
