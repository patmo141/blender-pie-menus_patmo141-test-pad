# -*- coding: utf-8 -*-
 
# Copyright (c) 2010, Dan Eicher.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
# <pep8 compliant>

bl_info = {
    "name": "Pie Library",
    "author": "Dan Eicher, Sean Olson",
    "version": (0, 1, 0),
    "blender": (2, 6, 4),
    "location": "",
    "description": "Library Used by Pie Menus",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "3D View"
}
 
import bpy
import math
import blf
import bgl
from mathutils import Vector, Matrix
import pie_menu_utils as pmu

class MenuItem(object):
    def __init__(self, id, x=0, y=0, rot=0, scale = 1, angle = math.pi/8, icon = "", poly_bound = []):
        self.id = id
        self.x = x
        self.y = y
        self.rot = rot
        self.scale = scale
        self.angle = angle  #1/2 * 2*pi/n => pi/n typically
        self.icon = icon
        self.icon_quad = pmu.make_quad(1,1,.5,.5,0) #later, this will be more flexible
        self.poly_bound = poly_bound
        self.calc_mode = "POLY_LOOP" #ANGLE, ICON_QUAD, POLY_LOOP
        
        #this will be update based on x,y, ang and scale only when self.update_local_to_screen
        #use these for drawing, but use self.calc to mouse test.  That way only the mouse coord
        #gets transformed for the test and you only have to recalc everything to draw when
        #position/loc/scale or menu changes.
        self.screen_icon_quad = []
        self.screen_poly_bound = []
 
    def op(self, parent, context):
        raise NotImplementedError
 
    def poll(self, context):
        return True
        
    def calc(self, mx, my):
        '''
        args: mx,my screen coordinates of mouse
        return self or none
        '''
        #put the mouse coordinates in item space
        mx -= self.x
        my -= self.y
        mouse = Vector((mx,my))
        rmatrix = Matrix.Rotation(-1*self.rot,2)
        mouse = (1/self.scale)*(rmatrix*mouse)
        
        if self.calc_mode == "POLY_LOOP":
            if pmu.point_inside_loop(self.poly_bound,mouse):
                #print(self.id)
                return True
            else: return False

        if self.calc_mode == "ICON_QUAD": #TODO, make this actually test based on the icon quad
            if pmu.point_inside_loop(self.poly_bound,mouse):
                #print(self.id)
                return True
            else: return False
            
    def update_local_to_screen(self):
        n = len(self.icon_quad)
        if n > 0:
            self.screen_icon_quad = [[0,1]]*n
            for i in range(0,n):
                #print(self.x)
                #print(self.y)
                #print(self.scale)
                #print(self.id)
                
                vec = Vector((self.icon_quad[i][0],self.icon_quad[i][1]))
                vec = self.scale*vec
                vec = Matrix.Rotation(self.rot,2)*vec
                vec += Vector((self.x,self.y))
                self.screen_icon_quad[i] = vec
        j = len(self.poly_bound)  
        if j > 0:
            self.screen_poly_bound = [[0,1]]*j
            for i in range(0,j):
                vec = Vector((self.poly_bound[i][0],self.poly_bound[i][1]))
                vec = self.scale*vec
                vec = Matrix.Rotation(self.rot,2)*vec
                vec += Vector((self.x,self.y))
                self.screen_poly_bound[i] = vec
    def icon_to_round(self):
        print('not implemented')
    
class PieMenu(object):
    
    def __init__(self, x, y, layout_radius, text_size,
                 text_dpi, center_radius_squared,
                 max_radius_squared, corner_radius=5, icon = ""):
        self.menu_x = x
        self.menu_y = y
        self.radius = layout_radius
        self.text_size = text_size
        self.text_dpi = text_dpi
        #self.center_radius = center_radius_squared  #use if each pie needs own inner radius setting
        self.center_radius = 900
        #self.max_radius = max_radius_squared #use if each pie needs own outer radius setting
        self.max_radius = bpy.context.scene.pieOuterRadius**2
        self.corner_radius = corner_radius
        self.angle = 0
        self.item_offset = 0
        self.menu_items = []
        self.vec_cache = None

        ui=bpy.context.user_preferences.themes['Default'].user_interface #alias
        self.ris = ui.wcol_pulldown.inner_sel[0]
        self.gis = ui.wcol_pulldown.inner_sel[1]
        self.bis = ui.wcol_pulldown.inner_sel[2]
        self.ais = ui.wcol_pulldown.inner_sel[3]
    
        self.ri = ui.wcol_menu.inner[0]
        self.gi = ui.wcol_menu.inner[1]
        self.bi = ui.wcol_menu.inner[2]
        self.ai = ui.wcol_menu.inner[3]

        self.ro = ui.wcol_menu.outline[0]
        self.go = ui.wcol_menu.outline[1]
        self.bo = ui.wcol_menu.outline[2]

        self.rt = ui.wcol_menu.text[0]
        self.gt = ui.wcol_menu.text[1]
        self.bt = ui.wcol_menu.text[2] 
    
        self.pointerangle=0
        

    ## auto-layout menu items around the base circle
    def layout_even(self, radians):
        angle = self.angle #this is a limiting angle...eg, only spread them over pi/4
        items = len(self.menu_items)
        if angle:  # keep sub-menu items inside main slice
            step = radians / (items)
            angle -= (radians * 0.5)
        else:
            step = radians / items
        self.item_offset = step * 0.5
 
        # Calculate pie slice
        for it in self.menu_items:
            it.rot = 0
            it.angle = self.item_offset  #To Do...self.item_offsst redundant each item has a width?
            # Rotate point around center with Fancy Math, set item x,y to screen
            it.x = self.radius * math.sin(angle) + self.menu_x
            it.y = self.radius * math.cos(angle) + self.menu_y
            it.update_local_to_screen()
            angle += step
            
    def layout_predefined(self):
    
        #it = MenuItem  #used for PyDev autocompletion
        for it in self.menu_items:
            it.x += self.menu_x
            it.y += self.menu_y
        
            it.update_local_to_screen()
 
    ## fetch the item under the pointer
    def calc_by_angle(self, x, y):
        # mouse position converted from screen space to menu space
        x -= self.menu_x
        y -= self.menu_y
 
        # Cursor in inactive regions
        rad = pow(x, 2) + pow(y, 2)
        if rad < self.center_radius:
            return None
        if rad > self.max_radius:
            return -1
 
        angle = -math.fmod(math.atan2(x, y) - 2*math.pi, 2*math.pi)
        
        
        for it in self.menu_items:
            
            item_angle = -math.fmod(math.atan2(it.x-self.menu_x, it.y-self.menu_y) - 2*math.pi, 2*math.pi)
            
            if (item_angle - it.angle) < angle <= (item_angle + it.angle):
                return it
        print("mouse_angle %f" % angle)
        print("item_angle %f" % item_angle)
        # special case for 12 o'clock position..dont know if we need this?
        if angle <= it.angle or angle > (2*math.pi - it.angle):
            return self.menu_items[0]
        else:  # should never make it this far but you never know
            return None
    
    def calc_by_item(self, x, y):
        for it in self.menu_items:
            if it.calc(x,y):
                return it
    # draw the menu
    def draw(self, parent, context):

        #spread the icons over 360 degrees evenly
        #self.layout(2*math.pi) #probably don't need to do this every draw...
        
        biggestdimensionX=biggestdimensionY=0
        for it in self.menu_items:
            #find the biggest word in the menu and base the size of all buttons on that word
            blf.size(0, self.text_size, self.text_dpi)
            dimension = blf.dimensions(0, it.id)
    
            if dimension[0]>biggestdimensionX:
                biggestdimensionX = dimension[0]
            if dimension[1]>biggestdimensionY:
                biggestdimensionY = dimension[1]

        for it in self.menu_items:
            sel = (it == parent.current)
            it_poll = it.poll(context)
 
            # center item on the circle
            #x = (self.menu_x + it.x) - (dimension[0] * 0.5)
            #y = (self.menu_y + it.y) - (dimension[1] * 0.5)

            blf.size(0, self.text_size, self.text_dpi)
            dimension = blf.dimensions(0, it.id)

            #needed for box centering
            x = (it.x) - (biggestdimensionX * 0.5)
            y = (it.y) - (biggestdimensionY * 0.5)

            #needed offset for text centering
            blf.size(0, self.text_size, self.text_dpi)
            dimension = blf.dimensions(0, it.id)
            xt = ((biggestdimensionX-dimension[0]) * 0.5)
            yt = ((biggestdimensionY-dimension[1]) * 0.5)

            # Draw background buttons
            if sel and it_poll:
                bgl.glColor4f(self.ris, self.gis, self.bis, self.ais)
            else:
                bgl.glColor4f(self.ri, self.gi, self.bi, self.ai)
 
            #self.gl_pie_slice(bgl.GL_POLYGON, 8, it, x,y,30,90) #***
            #http://www.opengl.org/archives/resources/faq/technical/rasterization.htm#rast0120
            self._round_box(bgl.GL_POLYGON, x - 20, y - 5, x + biggestdimensionX + 20, y + biggestdimensionY + 5)          
            if it.screen_poly_bound:
                shape = it.screen_poly_bound            
                pmu.draw_outline_or_region(bgl.GL_POLYGON, shape)
                bgl.glColor4f(self.ro, self.go, self.bo, 1.0)
                pmu.draw_outline_or_region(bgl.GL_LINE_LOOP, shape)
            
            
            #box outline 
            bgl.glColor4f(self.ro, self.go, self.bo, 1.0)
            #gl_pie_slice3(bgl.GL_LINE_LOOP, 8, it, x,y,30,90) #***
            self._round_box(bgl.GL_LINE_LOOP, x - 20, y - 5, x + biggestdimensionX + 20, y + biggestdimensionY + 5)
            
            bgl.glColor4f(self.ri, self.gi, self.bi, self.ai)

            #place the icon quad
            verts = it.screen_icon_quad
            #verts = pmu.make_quad(biggestdimensionX*2,biggestdimensionX*2, self.menu_x + it.x, self.menu_y + it.y,0)
            img = bpy.data.images.get(it.icon) #returns none if doesn't exist
            if img:
                color = (1,1,1,.5)
                pmu.image_quad(img, color, verts)
                
        

            #draw the circle
            if bpy.context.scene.clockBool:
                self._circle_(bgl.GL_TRIANGLE_FAN, (self.menu_x), (self.menu_y), 20)
            
                #draw the circle outline
                bgl.glColor4f(self.ro, self.go, self.bo, 1.0)
                self._circle_(bgl.GL_LINE_LOOP, (self.menu_x), (self.menu_y), 20)

                self._pointer_(bgl.GL_TRIANGLE_STRIP, (self.menu_x), (self.menu_y), self.pointerangle )
 
            # Draw text
            blf.enable(0, blf.SHADOW)
            blf.shadow_offset(0, 0, 0)
            blf.shadow(0, 5, 0.0, 0.0, 0.0, 1.0)
            if it_poll:
                bgl.glColor3f(self.rt, self.gt, self.bt)
            else:  # grayed out
                bgl.glColor3f(0.5, 0.5, 0.5)
            blf.position(0, x+xt, y+yt, 0)
            blf.draw(0, it.id)
            blf.disable(0, blf.SHADOW)
 
    ## nabbed from interface_draw.c

    def gl_pie_slice(self, mode, items, item, cx, cy, inner, outer): #***
        step = 360/items
        item -= 1
        bgl.glBegin(mode)
        for n in range(8):
            a = (item*360+180)/items+(n+8)*360/(items*15)
            x = math.sin(math.radians(a))
            y = math.cos(math.radians(a))
            bgl.glVertex2f(inner*x+cx,inner*y+cy)
        for n in range(16):
            a = ((item+1)*360+180)/items-n*360/(items*15)
            x = math.sin(math.radians(a))
            y = math.cos(math.radians(a))
            bgl.glVertex2f(outer*x+cx,outer*y+cy)
        for n in range(8):
            a = (item*360+180)/items+n*360/(items*15)
            x = math.sin(math.radians(a))
            y = math.cos(math.radians(a))
            bgl.glVertex2f(inner*x+cx,inner*y+cy)
        bgl.glEnd()

    def _circle_(self, mode, centerX, centerY, radius):
        bgl.glBegin(mode)
        for angle in range(0,20):
            angle = angle*2*math.pi/20 
            bgl.glVertex2f(centerX + (math.cos(angle) * radius), centerY + (math.sin(angle) * radius))
        bgl.glEnd()

    def _line_(self, mode, startX, startY, endX, endY):
        bgl.glBegin(bgl.GL_LINES)
        bgl.glVertex2f(startX, startY)
        bgl.glVertex2f(endX, endY)
        bgl.glEnd()

    def _pointer_(self, mode, centerX, centerY, rotation):
        bgl.glBegin(mode);
        bgl.glColor4f(self.ris, self.gis, self.bis, self.ais)

        def draw_rotated(cos,sin, centerx, centery, x, y):
            nx = centerx + ((x * cos) - (y * sin))
            ny = centery + ((x * sin) + (y * cos))
            bgl.glVertex2f(nx,ny)

        sin = math.sin(math.radians(rotation))
        cos = math.cos(math.radians(rotation))

        draw_rotated(cos, sin, centerX, centerY, 0, 24)
        draw_rotated(cos, sin, centerX, centerY, -5, 14)
        draw_rotated(cos, sin, centerX, centerY, 5, 14)
        draw_rotated(cos, sin, centerX, centerY, -3, 10)
        draw_rotated(cos, sin, centerX, centerY, 3, 10)


        bgl.glPopMatrix()
        bgl.glEnd()


    def _round_box(self, mode, minx, miny, maxx, maxy):
       
        rad = self.corner_radius
        vec = [[0.195, 0.02],
               [0.383, 0.067],
               [0.55, 0.169],
               [0.707, 0.293],
               [0.831, 0.45],
               [0.924, 0.617],
               [0.98, 0.805]]
 
        # cache vec list to save 224 multiplications per 8 item menu redraw
        if self.vec_cache == None:
            self.vec_cache = [(point[0] * rad, point[1] * rad) for point in vec]
 
        bgl.glBegin(mode)
        #http://www.opengl.org/discussion_boards/showthread.php/164767-Newb-glVertex2fv(vertices-edges-i-1-)
        # start with corner right-bottom
        bgl.glVertex2f(maxx - rad, miny)
        bgl.glVertex2fv(bgl.Buffer(bgl.GL_FLOAT, [7,2],
                                   [(maxx - rad + point[0], miny + point[1]) for point in self.vec_cache]))
        bgl.glVertex2f(maxx, miny + rad)
 
        # corner right-top
        bgl.glVertex2f(maxx, maxy - rad)
        bgl.glVertex2fv(bgl.Buffer(bgl.GL_FLOAT, [7,2],
                                   [(maxx - point[1], maxy - rad + point[0]) for point in self.vec_cache]))
        bgl.glVertex2f(maxx - rad, maxy)
 
        # corner left-top
        bgl.glVertex2f(minx + rad, maxy)
        bgl.glVertex2fv(bgl.Buffer(bgl.GL_FLOAT, [7,2],
                                   [(minx + rad - point[0], maxy - point[1]) for point in self.vec_cache]))
        bgl.glVertex2f(minx, maxy - rad)
 
        # corner left-bottom
        bgl.glVertex2f(minx, miny + rad)
        bgl.glVertex2fv(bgl.Buffer(bgl.GL_FLOAT, [7,2],
                                   [(minx + point[1], miny + rad - point[0]) for point in self.vec_cache]))
        bgl.glVertex2f(minx + rad, miny)
 
        bgl.glEnd()



