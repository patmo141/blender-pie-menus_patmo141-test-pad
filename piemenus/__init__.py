# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####


""" Copyright 2011 GPL licence applies"""

bl_info = {
    "name": "Pie: Menus",
    "description": "Pie Menus for various functionality",
    "author": "Dan Eicher, Sean Olson",
    "version": (1, 0, 1),
    "blender": (2, 6, 5),
    "location": "View3D - Set Keybindings in File->Userprefs->Input Tab->(Search: \"pie\")",
    "warning": '',  # used for warning icon and text in addons panel
    "wiki_url": "",
    "tracker_url": "",
    "category": "3D View"}

#we need to add the piemenus subdirectory to be searched for imports
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'piemenus'))

#this is the guts of the script
import pie_menu
import pie_menu_utils
#import the individual menus
import pie_viewMenu
import pie_modeMenu
import pie_selectionMenu
import pie_shadeMenu
import pie_pivotMenu
import pie_strokesMenu
import pie_sculptTextureMenu
import pie_greyBrushes
import pie_redBrushes
import pie_tanBrushes
import pie_deleteMenu

import bpy
from bpy.props import *
from bpy.app.handlers import persistent
from rna_prop_ui import PropertyPanel

 
#collection class to keep track of individual pie menu variables
class PiePropertyGroup(bpy.types.PropertyGroup):
    name = StringProperty(default="")    
    activate = BoolProperty(default=True)

#initialization script
def initSceneProperties(scn):
    if not bpy.context.scene.pie_settings:
        pie_set = bpy.context.scene.pie_settings
        
        pie_item = pie_set.add()
        pie_item.name = "[ON] 3DView - View Menu"

        pie_item = pie_set.add()
        pie_item.name = "[ON] 3DView - Mode Menu"

        pie_item = pie_set.add()
        pie_item.name = "[ON] 3DView - Shade Menu"

        pie_item = pie_set.add()
        pie_item.name = "[ON] 3DView - Pivot Menu"

        pie_item = pie_set.add()
        pie_item.name = "[ON] Edit - Delete Menu"

        pie_item = pie_set.add()
        pie_item.name = "[ON] Edit - Selection Menu"

        pie_item = pie_set.add()
        pie_item.name = "[ON] Sculpt - Grey Brushes Menu"

        pie_item = pie_set.add()
        pie_item.name = "[ON] Sculpt - Red Brushes Menu"

        pie_item = pie_set.add()
        pie_item.name = "[ON] Sculpt - Tan Brushes Menu"

        pie_item = pie_set.add()
        pie_item.name = "[ON] Sculpt - Texture Menu"

        pie_item = pie_set.add()
        pie_item.name = "[ON] Sculpt - Strokes Menu"

    bpy.types.Scene.preferences = BoolProperty(name = "Preferences", description = "Pie Menu Preferences")
    scn['preferences'] = True

    bpy.types.Scene.clockBool = BoolProperty(name = "Center Widget", description = "Turn on/off Center Widget")
    scn['clockBool'] = True

    bpy.types.Scene.pieOuterRadius = IntProperty(name = "Pie Outer Radius", description = "Amount that mouse can travel beyond pies before deactivating menu")
    scn['pieOuterRadius'] = 300 

    return


# this the main properties panel panel
class piemenus_panel(bpy.types.Panel):
    bl_label = "Pie Menus"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_options = {'DEFAULT_CLOSED'}   
      

    def draw(self, context):
        scn = context.scene
        layout = self.layout
        #split = layout.split()
 
        box = layout.box().column(align=False)
        box.prop(scn,'preferences')
        if scn.preferences:
            boxrow=box.row()
            boxrow.template_list(context.scene, "pie_settings", context.scene, "pie_settings_index", prop_list="template_list_controls", rows=5)
            
            boxrow=box.row()
            subrow = boxrow.row(align=True)
            subrow.operator("activatepie.button", text="On")
            subrow.operator("disactivatepie.button", text="Off")
            
            boxrow=box.row()
            boxrow.operator("pie.keybinding", text="Keybindings")

            boxrow=box.row()
            boxrow.label(text="General Preferences")

            boxrow=box.row()
            boxrow.prop(scn, 'clockBool')
            
            boxrow=box.row()
            boxrow.prop(scn, 'pieOuterRadius')
#Button to pull up keybindings window with relevent keybindings shown            
class OBJECT_OT_KeybindsButton(bpy.types.Operator):
    bl_idname = "pie.keybinding"
    bl_label = "View Pie Menu Keybinds"

    def execute(self, context):
        bpy.ops.screen.userpref_show('INVOKE_DEFAULT')
        bpy.context.user_preferences.active_section='INPUT'
        bpy.context.window_manager.windows[1].screen.areas[0].spaces[0].filter_text='Pie'
        return{'FINISHED'}  

#Button to Activate Pie Menus
class OBJECT_OT_ActivatePieButton(bpy.types.Operator):
    bl_idname = "activatepie.button"
    bl_label = "On"
     
    def execute(self, context):
        bpy.context.scene.pie_settings[bpy.context.scene.pie_settings_index].activate=True
        bpy.context.scene.pie_settings[bpy.context.scene.pie_settings_index].name=bpy.context.scene.pie_settings[bpy.context.scene.pie_settings_index].name.replace('[OFF]', '[ON]')
        updateBinds()
        return{'FINISHED'}

#Button to Deactivate Pie Menus
class OBJECT_OT_DisactivatePieButton(bpy.types.Operator):
    bl_idname = "disactivatepie.button"
    bl_label = "Off"
     
    def execute(self, context):
        bpy.context.scene.pie_settings[bpy.context.scene.pie_settings_index].activate=False
        bpy.context.scene.pie_settings[bpy.context.scene.pie_settings_index].name=bpy.context.scene.pie_settings[bpy.context.scene.pie_settings_index].name.replace('[ON]', '[OFF]')
        updateBinds()
        return{'FINISHED'}

#Updated all the keybindings
def updateBinds(): 
    tempstring=StringProperty()

    for i in range(len(bpy.context.scene.pie_settings)):
        name=bpy.context.scene.pie_settings[i].name
        if not name.find('View Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_viewMenu.setBind()
            else:               
                pie_viewMenu.removeBind()

        if not name.find('Mode Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_modeMenu.setBind()
            else:               
                pie_modeMenu.removeBind()

        if not name.find('Shade Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_shadeMenu.setBind()
            else:               
                pie_shadeMenu.removeBind()

        if not name.find('Pivot Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_pivotMenu.setBind()
            else:               
                pie_pivotMenu.removeBind()

        if not name.find('Delete Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_deleteMenu.setBind()
            else:               
                pie_deleteMenu.removeBind()

        if not name.find('Selection Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_selectionMenu.setBind()
            else:               
                pie_selectionMenu.removeBind()

        if not name.find('Strokes Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_strokesMenu.setBind()
            else:               
                pie_strokesMenu.removeBind()

        if not name.find('Texture Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_sculptTextureMenu.setBind()
            else:               
                pie_sculptTextureMenu.removeBind()
        
        if not name.find('Grey Brushes Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_greyBrushes.setBind()
            else:               
                pie_greyBrushes.removeBind()

        if not name.find('Red Brushes Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_redBrushes.setBind()
            else:               
                pie_redBrushes.removeBind()

        if not name.find('Tan Brushes Menu')==-1:
            if bpy.context.scene.pie_settings[i].activate:               
                pie_tanBrushes.setBind()
            else:               
                pie_tanBrushes.removeBind()
        

               
def register():    
    bpy.utils.register_class(PiePropertyGroup)
    
    bpy.utils.register_module(__name__)

    pie_menu_utils.register()
    pie_modeMenu.register()             
    pie_selectionMenu.register() 
    pie_deleteMenu.register()        
    pie_viewMenu.register()             
    pie_shadeMenu.register()            
    pie_pivotMenu.register()            
    pie_strokesMenu.register()          
    pie_sculptTextureMenu.register()    
    pie_greyBrushes.register()
    pie_redBrushes.register()
    pie_tanBrushes.register()
    
    #places to store stuff
    bpy.types.Scene.pie_settings = CollectionProperty(type=PiePropertyGroup)
    bpy.types.Scene.pie_settings_index = IntProperty()

    initSceneProperties(bpy.context.scene)
    updateBinds()
   
def unregister():
    bpy.utils.unregister_module(__name__)

    pie_menu_utils.unregister()
    pie_modeMenu.unregister()
    pie_selectionMenu.unregister()
    pie_deleteMenu.unregister()  
    pie_viewMenu.unregister()
    pie_shadeMenu.unregister()
    pie_pivotMenu.unregister()
    pie_strokesMenu.unregister()
    pie_sculptTextureMenu.unregister()
    pie_greyBrushes.unregister()
    pie_redBrushes.unregister()
    pie_tanBrushes.unregister()

 
if __name__ == "__main__":
    register()
