# -*- coding: utf-8 -*-
 
# Copyright (c) 2010, Dan Eicher.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
# <pep8 compliant>

import bpy
from pie_menu import MenuItem, PieMenu
from pie_menu_utils import icons_to_blend_data

import math
import blf
import bgl
import os
from addon_utils import paths
 
bl_info = {
    "name": "Pie: ViewMenu",
    "author": "Dan Eicher, Sean Olson",
    "version": (0, 1, 0),
    "blender": (2, 6, 4),
    "location": "View3D",
    "description": "3d View view modes pie menu",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "3D View"
}
 
class VIEW3D_MT_View_Menu(bpy.types.Operator):
    '''View Menu'''
    bl_idname = "view3d.view_menu"
    bl_label = "Pie View Menu"
 
    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D'
        

 
    def modal(self, context, event):
        context.area.tag_redraw()
 
        if event.type == 'MOUSEMOVE':
            # mouse-over highlight

            #self.menu = PieMenu
            self.current = self.menu.calc_by_angle(event.mouse_region_x, event.mouse_region_y)
                        
            # pointer of center circle points at the mouse
            self.menu.pointerangle=math.degrees(math.atan2(event.mouse_region_y-self.menu.menu_y, event.mouse_region_x-self.menu.menu_x))-90     
            

        elif event.type == 'LEFTMOUSE':
            # get menu item clicked
            self.current = self.menu.calc_by_angle(event.mouse_region_x,
                                          event.mouse_region_y)
 
            do_exec = self.current and self.current.poll(context)
            if do_exec:
                self.current.op(self, context)
 
            if self.current is None or do_exec:
                context.region.callback_remove(self._handle)
                return {'FINISHED'}
 
        if self.current == -1 or event.type in ('RIGHTMOUSE', 'ESC'):
            context.region.callback_remove(self._handle)
            return {'CANCELLED'}
 
        return {'RUNNING_MODAL'}
 
    def invoke(self, context, event):
        self.current = None

        # generate menu content
        self.menu = menu_init(PieMenu(x=event.mouse_region_x,
                                      y=event.mouse_region_y,
                                      layout_radius=80,
                                      text_size=11,
                                      text_dpi=72,
                                      center_radius_squared=225,
                                      max_radius_squared=62500#22500
                                      ))
 
        context.window_manager.modal_handler_add(self)
        #self.menu.layout(2*math.pi)  commented out to preserve hand set layouts
        self._handle = context.region.callback_add(self.menu.draw,
                                                   (self, context),
                                                   'POST_PIXEL')
        return {'RUNNING_MODAL'}
 
 
def menu_init(menu):
    #ViewTop.__init__(  #uncomment to see PyDev auto prediction
    #ViewTop.__init__(self, id, x, y, rot, scale, angle, icon, poly_bound)
    menu.menu_items.append(ViewTop("Top", 0, 55, 0))  
    menu.menu_items.append(ViewFront("Front", 65, 30, 0))
    menu.menu_items.append(ViewRight("Right", 80, 0, 0))
    menu.menu_items.append(ViewCamera("Camera", 65, -30, 0))
    menu.menu_items.append(ViewBottom("Bottom", 0, -55, 0))
    menu.menu_items.append(ViewSelected("Selected", -65, -30, 0))
    menu.menu_items.append(ViewLeft("Left", -80, 0, 0))
    menu.menu_items.append(ViewBack("Back", -65, 30, 0))
 
    menu.item_offset = math.pi/8
    menu.layout_predefined()
    return menu
 
 
class ViewFront(MenuItem):
    def op(self, parent, context):
        bpy.ops.view3d.viewnumpad(type='FRONT', align_active=False)
         
 
 
class ViewTop(MenuItem):
    def op(self, parent, context):
        bpy.ops.view3d.viewnumpad(type='TOP', align_active=False)
 
 
class ViewRight(MenuItem):
    def op(self, parent, context):
        bpy.ops.view3d.viewnumpad(type='RIGHT', align_active=False)
 
 
class ViewCamera(MenuItem):
    def op(self, parent, context):
        bpy.ops.view3d.viewnumpad(type='CAMERA', align_active=False)
        

class PerspectiveToggle(MenuItem):
    def op(self, parent, context):
        bpy.ops.view3d.view_persportho()
 
class ViewBack(MenuItem):
    def op(self, parent, context):
        bpy.ops.view3d.viewnumpad(type='BACK', align_active=False)
 
 
class ViewBottom(MenuItem):
    def op(self, parent, context):
        bpy.ops.view3d.viewnumpad(type='BOTTOM', align_active=False)
 
 
class ViewLeft(MenuItem):
    def op(self, parent, context):
        bpy.ops.view3d.viewnumpad(type='LEFT', align_active=False)
       

class ViewSelected(MenuItem):
    def op(self, parent, context):
        bpy.ops.view3d.view_selected()
 

def setBind():
    km = bpy.context.window_manager.keyconfigs.active.keymaps['3D View']
    for kmi in km.keymap_items:
        if kmi.idname == 'view3d.view_menu':
            if kmi.type == 'Q' and kmi.ctrl==False and kmi.alt==False and kmi.shift==False and kmi.oskey==False and kmi.any==False and kmi.key_modifier=='NONE':
                kmi.active=True
                break

def removeBind():
    #replace default
    km = bpy.context.window_manager.keyconfigs.active.keymaps['3D View']
    for kmi in km.keymap_items:
        if kmi.idname == 'view3d.view_menu':
            if kmi.type == 'Q' and kmi.ctrl==False and kmi.alt==False and kmi.shift==False and kmi.oskey==False and kmi.any==False and kmi.key_modifier=='NONE':
                kmi.active=False
                break
    

def register():
    bpy.utils.register_class(VIEW3D_MT_View_Menu)
    km = bpy.context.window_manager.keyconfigs.active.keymaps['3D View']
    km.keymap_items.new('view3d.view_menu', 'Q', 'PRESS')



 
def unregister():
    bpy.utils.unregister_class(VIEW3D_MT_View_Menu)

    km = bpy.context.window_manager.keyconfigs.active.keymaps['3D View']
    for kmi in km.keymap_items:
        if kmi.idname == 'view3d.view_menu':
            km.keymap_items.remove(kmi)
            break
 
if __name__ == "__main__":
    register()
